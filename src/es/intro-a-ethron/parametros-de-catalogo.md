# Parámetros de catálogo

*Tiempo estimado de lectura: 3min*

Un **parámetro** (*parameter*) es un dato que puede definirse y reemplazarse en un archivo.
Se pueden referenciar en los archivos de catálogo, y sólo en valores de tipo texto, como `${parámetro}`.
Un valor puede referenciar a tantos parámetros como sea necesario.

El siguiente es un ejemplo de un proyecto de documentación como éste:

```yaml
version: 0.1.0

params:
  - param: version
    value: 0.13.0
    desc: The current version of the product.

  - param: years
    value: 2015-2020
    desc: The years dedicated to the project.
  
  - param: productName
    value: EthronLabs
    desc: The product name.

  - param: src
    value: ./src/
    desc: The dir containing the source files.

  - param: dst
    value: ./dist/
    desc: The dir where the final files are generated.
  
defaultTask: build

tasks:
  - macro: build
    title: Build the dist dir
    tasks:
      - [fs.rm, "${dst}"]
      - [fs.mkdir, "${dst}"]
      - [handlebars.render, "${src}", "${*}", "${dst}"]
```

Hay que tener en cuenta lo siguiente:

- Si el valor contiene un parámetro y algo más como, p.ej., `${archivo}.txt`, el valor seguirá siendo de tipo texto.

- Si el valor contiene varios parámetros como, p.ej., `${archivo}${extension}`, el valor seguirá siendo de tipo texto.

- Si el valor es simplemente un parámetro como, p.ej., `${tags}`, el valor se convertirá al del parámetro.
  De tal forma que si el valor del parámetro es una lista, el valor que inicialmente es de tipo texto se convertirá a lista.

- Se puede usar `${*}` para recibir un objeto con los parámetros y sus valores.
  Es muy útil si estamos usando un sistema de plantillas como [Handlebars](https://handlebarsjs.com).

Se distingue entre parámetros predefinidos, aquellos que vienen de fábrica, y los personalizados, aquellos que definimos nosotros mismos.

### Parámetros predefinidos

Un **parámetro predefinido** (*built-in parameter*) es aquel que define el propio sistema, es decir, lo define `ethron`.

P.ej., cuando se usa un archivo de catálogo local, tenemos los siguientes parámetros predefinidos:

- `workDir` (texto), ruta absoluta al directorio de trabajo.

- `dir` (texto), ruta absoluta al directorio en el que se encuentra el archivo.

- `file` (texto), ruta absoluta al archivo.

- `fileName` (texto), nombre del archivo con extensión, pero sin ruta padre.

Adicionalmente, también tenemos los siguientes:

- `today` (texto), la fecha de hoy como, p.ej., *Sun Apr 19 2020*.

### Parámetros personalizados

Un **parámetro personalizado** (*custom parameter*) es aquel que definimos nosotros mismos.
A diferencia de los predefinidos que vienen de fábrica.

#### Parámetros definidos mediante variables de entorno

Por un lado, cualquier variable de entorno que comience por el prefijo `ETHRON_PARAM_` se añadirá al objeto parámetros.
Y se hará sin el prefijo.
P.ej., si tenemos la variable de entorno `ETHRON_PARAM_xyz`, se añadirá como `xyz`.

Puede utilizar el comando `ethron env` para conocer si hay parámetros definidos.
Ejemplo:

```
$ export ETHRON_PARAM_welcome='ciao!'
$ ethron env

 Variable              Value  Desc.                              
-----------------------------------------------------------------
 ETHRON_CAT            cat    The default catalog to use.        
 ETHRON_CONF                  The default configuration to use.  
 ETHRON_TASK                  The default cataloged task to use. 
 ETHRON_PARAM_welcome  ciao!  A parameter to use.                

$
```

#### Parámetros definidos en la línea de comandos

Mediante la opción `-p` se puede definir un parámetro.
Observe que es una *p* minúscula.
Ejemplo:

```
$ ethron -p param1=valor1 -p param2=valor2
```

#### Parámetros definidos en los propios archivos

Como puede observar en el archivo de ejemplo al comienzo de la lección, se puede definir parámetros mediante el campo `params`.
Consiste en una lista, donde cada uno de sus elementos es de tipo objeto y representa un parámetro.
Cada uno de ellos, dispone de los siguientes campos:

- `param` (texto, obligatorio), nombre del parámetro.

- `desc` (texto, opcional), breve descripción del parámetro.

- `value` (obligatorio), valor del parámetro.

Adicionalmente, el campo `params` puede contener elementos `init` que contienen una ruta a un archivo de **JavaScript**
en el cual definir parámetros adicionales que, p.ej., puedan depender de la plataforma.
El archivo **JavaScript** puede devolver un objeto cuyo contenido se sumará a los parámetros ya definidos,
o bien una función la cual se invocará y devolverá los parámetros adicionales.
En este segundo caso, la signatura de la función es como sigue:

```
function(): Object
```

Ejemplo de una definición en un archivo YAML:

```
params:
  - param: tmpDir
    desc: Directorio donde almacenar archivos temporales.
    value: /tmp

  - param: httpServer
    desc: Servidor de HTTP al que conectar.
    value: mi.servidor.com

  - init: ./params/platform.js
    desc: Parámetros cuyo valor depende de la plataforma.
```

Y aquí un ejemplo de `./params/platform.js`:

```
const os = require("os");

module.exports = exports = {
  platform: os.platform(),
  home: os.homedir()
};
```

Para comprender mejor cómo funciona todo, hay que tener claro el proceso de lectura de un archivo:

1. Se lee el contenido del archivo, sin realizar ningún tipo de cambio.
   Esto permite poder definir `params` en cualquier lugar del archivo, según los convenios internos de la organización.

2. Teniendo en cuenta los parámetros pasados en la línea de comandos, en variables de entorno, los predefinidos y los definidos en `params`,
   se pasa a sustituirlos en los valores de tipo texto del archivo.