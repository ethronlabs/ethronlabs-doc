# Introducción a las tareas

*Tiempo estimado de lectura: 4min*

Una **tarea** (*task*) representa un trabajo o actividad a ejecutar como, p.ej.:

- Una operación de compilación.

- La minimización de código JavaScript, HTML o CSS.

- La realización de una copia de seguridad.

- El alta de un nuevo usuario.

- La ejecución de un administrador de paquetes como, p.ej., APT, APK, NPM o LuaRocks.

- El inicio de uno o más contenedores de Docker.

- El inicio de una instancia de base de datos local.

Las tareas se clasifican en simples, compuestas y alias.

## Tareas simples

Una **tarea simple** (*simple task*) es la unidad de operación más pequeña que tiene como objeto realizar un determinado trabajo indivisible.
En **{{productName}}**, se implementa mediante una función **JavaScript**.
Su ejecución la lanza el ejecutor de tareas, el cual recopilará información sobre su duración y resultado final.
A medida que el ejecutor ejecuta una tarea, va generando notificaciones que pueden procesar otros componentes, p.ej., para mostrar la evolución al usuario o incluso registrarla en una base de datos o en un flujo de **Redis Streams**.
De manera predeterminada, se usa el notificador de consola que muestra: cuándo comienza una tarea, cuánto ha tardado y cómo ha finalizado.
Para no perder el foco, vamos a mostrar la ejecución de una tarea de un catálogo local, es decir, ubicado en el disco:

```
$ ethron 

Catalog: ./Ethron.cat.yaml

 Task              Group  Desc.                                                        
---------------------------------------------------------------------------------------
 build             build  Build package                                                
 build-and-test *  test   Build and test (Redis must be started)                       
 ci                ci     Local Continuous Integration (for running before committing) 
 lint              build  Lint source code                                             
 redis/start       test   Start Redis                                                  
 redis/stop        test   Stop Redis                                                   
 test              test   Run unit tests (Redis must be started)                       

$ ethron r build


build
-----
Build package
  Lint source code
    shell: execute 'dogmac check src' ok (46 ms)
  Transpile from Dogma to JS
    fs: remove 'build' ok (3 ms)
    shell: execute 'dogmac js -o build src/' ok (50 ms)
    shell: execute 'dogmac js -o build test/' ok (82 ms)
  Transpile from JS to JS
    fs: remove 'lib' ok (1 ms)
    fs: remove '__test__' ok (1 ms)
    babel: transpile 'build/src' to 'lib/' ok (544 ms)
    babel: transpile 'build/test' to '__test__/' ok (92 ms)
  fs: copy 'package.json' to 'lib/package.json' ok (2 ms)
  fs: copy 'README.md' to 'lib/README.md' ok (1 ms)

ok       10
failed   0
skipped  0
total    10
$ 
```

Para toda tarea se indica:

- Su duración.
  Así, p.ej., la tarea simple *fs: remove 'build'* ha durado 3ms.
  En nuestro caso, esa tarea se encuentra dentro de otra compuesta, *Transpile from Dogma to JS*.

- Cómo finalizó:

  - *ok*, acabó sin error.

  - *failed*, acabó en error.

  - *skipped*, se omitió su ejecución porque así se indicó.
    P.ej., porque la tarea no debe ejecutarse en un entorno bajo determinada configuración.

## Tareas compuestas

Por su parte, una **tarea compuesta** (*composite task*) es aquella que está formada por cero, una o más tareas (simples y/o compuestas).
Ejemplos de este tipo de tarea son las macros y los generadores.
Ahora, vamos a introducir las macros, dejando para lecciones posteriores los demás tipos.

### Macros

Una **macro** es una secuencia ordenada de cero, una o más tareas.
Cada vez que se invoca la macro, se ejecuta cada una de sus tareas en el orden indicado.
En los catálogos, son el tipo de tarea más común.
Por esta razón, vamos a ver un catálogo lo que permitirá ver más claramente en qué consisten:

```yaml
version: 1.0.0

params:
  - param: dockerfile
    value: ./Dockerfile

  - param: image
    value: cache-for-redis

  - param: container
    value: cache-for-redis

defaultTask: build-and-test

tasks:
  #########
  # build #
  #########

  - alias: lint
    title: Lint source code
    group: build
    task: exec
    args: dogmac check src test

  - macro: trans/dogma
    title: Transpile from Dogma to JS
    group: build
    hidden: true
    tasks:
      - [fs.rm, build]
      - [exec, "dogmac js -o build src/"]
      - [exec, "dogmac js -o build test/"]

  - macro: trans/js
    title: Transpile from JS to JS
    group: build
    hidden: true
    tasks:
      - [fs.rm, lib]
      - [fs.rm, __test__]
      - [babel, build/src, lib/]
      - [babel, build/test, __test__/]

  - macro: build
    title: Build package
    group: build
    tasks:
      - lint
      - trans/dogma
      - trans/js
      - [fs.cp, package.json, lib/package.json]
      - [fs.cp, README.md, lib/README.md]

  ########
  # test #
  ########

  - alias: redis/start
    title: Start Redis
    group: test
    task: docker.run
    args:
      - redis:6-alpine
      - redis
      - 6379:6379
      - --detach
      - --rm

  - alias: redis/stop
    title: Stop Redis
    group: test
    task: docker.stop
    args: redis

  - tests: test
    title: Run unit tests (Redis must be started)
    group: test
    kind: unit
    workDir: __test__/unit
    tasks:
      - /

  - macro: build-and-test
    title: Build and test (Redis must be started)
    group: test
    tasks:
      - build
      - test

  ######
  # CI #
  ######

  - macro: ci/local
    title: Run the tests locally
    group: ci
    hidden: true
    tasks:
      - build
      - redis/stop
      - redis/start
      - test
      - redis/stop

  - macro: ci/docker
    title: Run the tests into a Docker container
    group: ci
    hidden: true
    tasks:
      - build
      - [docker.rm, "${container}"]
      - [docker.rmi, "${image}"]
      - [docker.build, "${dockerfile}", "${image}"]
      - [docker.run, "${image}", "${container}", 6379:6379]
      - [docker.logs, "${container}", " ok (", true, true]
      - [docker.logs, "${container}", " failed (", false, true]

  - macro: ci
    title: Local Continuous Integration (for running before committing)
    group: ci
    tasks:
      - ci/local
      - ci/docker
```

En el caso de un catálogo, las tareas que debe ejecutar la macro son las indicadas en su campo `tasks`.
En la siguiente lección, se presenta el concepto de catálogo local, lo que le ayudará a empezar a sacar todo el jugo a **{{productName}}**.

## Alias

Un **alias** no es más que la declaración de una invocación de tarea con unos determinados argumentos.
He aquí unos ejemplos ilustrativos:

```yaml
- alias: redis/start
  title: Start Redis
  group: test
  task: docker.run
  args:
    - redis:6-alpine
    - redis
    - 6379:6379
    - --detach
    - --rm

- alias: redis/stop
  title: Stop Redis
  group: test
  task: docker.stop
  args: redis
```

En `task`, se indica la tarea a ejecutar.
Y si hay que pasarle argumentos, los podemos indicar en `args` como:

- Un texto, indicando así una lista de un único argumento.
  Como en `redis/stop`.

- Una lista de argumentos.
  Como en `redis/start`.

- Un objeto de argumentos.