# Plugins predefinidos

*Tiempo estimado de lectura: 2min*

En software, es muy común desarrollar componentes reutilizables con objeto de no reinventar la rueda una y otra vez.
Mejorando así el rendimiento, la calidad y la robustez, entre otras cosas.
**{{productName}}** permite extender su funcionalidad mediante catálogos y *plugins*.
Los catálogos ya los hemos presentado.
Vamos, entonces, a presentar en familia a los *plugins*.

## Qué es un *plugin*

Un **plugin** es un componente que proporciona funcionalidad adicional.
Concretamente, una colección de una o más tareas para su reutilización.

Se distingue entre plugins predefinidos, aquellos que vienen con `ethron`;
y los personalizados, aquellos que hay que instalarlos e importarlos para poder usarlos.
En esta lección, nos centraremos en los predefinidos, dejando para la siguiente los restantes.

## Plugins predefinidos

Un **plugin predefinido** (*built-in plugin*) es aquel que viene de fábrica con `ethron`.
No hay que dárselos a conocer a `ethron` mediante el campo `plugins` porque ya sabe de su existencia.
Así que podemos utilizarlos sin más.

A continuación, se lista los *plugins* predefinidos:

- `@ethronlabs/pi-babel` para ejecutar [Babel](https://babeljs.io) fácilmente.

- `@ethronlabs/pi-docker` para ejecutar comandos de [Docker](https://www.docker.com).

- `@ethronlabs/pi-env` para crear, asignar o comprobar variables de entorno.

- `@ethronlabs/pi-exec` para ejecutar comandos de *shell* como, p.ej., `apt`.

- `@ethronlabs/pi-fs` para realizar operaciones con el sistema de ficheros como, p.ej., copiar o borrar archivos.

- `@ethronlabs/pi-handlebars` para trabajar con el motor de plantillas [Handlebars](https://handlebarsjs.com).

- `@ethronlabs/pi-sleep` para detener la ejecución durante un determinado periodo de tiempo.

- `@ethronlabs/pi-yaml` para trabajar con archivos YAML, p.ej., validando un archivo.

## Uso de plugins

La idea de los *plugins* es ampliar el conjunto de tareas disponible.
Para ello, las tareas se agrupan en *plugins*, se dan a conocer y se usan cuando es necesario.

En el caso de las tareas de los *plugins* predefinidos no hay más que usar una sintaxis como la siguiente:

```
nombrePlugin.nombreTarea
```

Veamos algunos ejemplos:

```yaml
- macro: build
  title: Build the dist dir
  tasks:
    - [fs.rm, "${dst}"]
    - [fs.mkdir, "${dst}"]
    - [handlebars.render, "${src}", "${*}", "${dst}"]
```

Cuando un *plugin* tiene una única tarea, el *plugin* puede invocarse sin necesidad de indicar explícitamente la tarea.
Este es el caso de los *plugins* `babel`, `exec` y `sleep`.
De ahí que podamos usarlos como muestran los siguientes ejemplos:

```yaml
- macro: trans/dogma
  title: Transpile from Dogma to JS
  group: build
  hidden: true
  tasks:
    - [fs.rm, build]
    - [exec, "dogmac js -o build src/"]
    - [exec, "dogmac js -o build test/"]

- macro: trans/js
  title: Transpile from JS to JS
  group: build
  hidden: true
  tasks:
    - [fs.rm, lib]
    - [fs.rm, __test__]
    - [babel, build/src, lib/]
    - [babel, build/test, __test__/]
```