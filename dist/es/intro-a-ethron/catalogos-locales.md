# Catálogos locales

*Tiempo estimado de lectura: 6min*

Un **catálogo de tareas** (*task catalog*), o simplemente **catálogo** (*catalog*), es un contenedor de tareas invocables desde la línea de comandos.
Es uno de los medios a través de los cuales se puede extender la funcionalidad de **EthronLabs**.
La idea de un catálogo es proporcionar tareas relacionadas con alguna cosa;
como, p.ej., la instalación, la desinstalación o la configuración predeterminada de un determinado producto.

Se definen de manera muy sencilla con **YAML**, **JSON** y/o **JavaScript**.
Cuando se trata de un catálogo local, se suele usar [YAML](https://yaml.org/), por lo que usaremos este lenguaje en esta lección.

Se distingue entre catálogos locales o integrados y catálogos empaquetados o NPM.
En esta lección, vamos a presentar los catálogos locales.

## Catálogos locales o integrados

Un **catálogo local** (*local catalog*) o **catálogo integrado** (*embedded catalog*) es aquel que se encuentra dentro del directorio actual.
Tienen como objeto automatizar tareas comunes de un determinado proyecto.
Así, p.ej., en un proyecto de desarrollo de software se puede utilizar un catálogo con tareas comunes de compilación, pruebas, empaquetado, etc.
En un proyecto de documentación como éste, se puede utilizar para generar la documentación final;
es más, este proyecto usa **EthronLabs** para hacerlo.

### Archivo de catálogo

Todo catálogo local se define mediante un **archivo de catálogo** (*catalog file*), un archivo **YAML** que define las tareas que contiene el catálogo.
Generalmente, estas tareas vienen definidas mediante *plugins*, las cuales expone el catálogo, en muchas ocasiones, con determinados parámetros.

Nada mejor que un ejemplo para ir abriendo boca:

```yaml
###################
# Ethron.cat.yaml #
###################

version: 1.0.0

params:
  - param: dockerfile
    value: ./Dockerfile

  - param: image
    value: cache-for-redis

  - param: container
    value: cache-for-redis

defaultTask: build-and-test

tasks:
  #########
  # build #
  #########

  - alias: lint
    title: Lint source code
    group: build
    task: exec
    args: dogmac check src test

  - macro: trans/dogma
    title: Transpile from Dogma to JS
    group: build
    hidden: true
    tasks:
      - [fs.rm, build]
      - [exec, "dogmac js -o build src/"]
      - [exec, "dogmac js -o build test/"]

  - macro: trans/js
    title: Transpile from JS to JS
    group: build
    hidden: true
    tasks:
      - [fs.rm, lib]
      - [fs.rm, __test__]
      - [babel, build/src, lib/]
      - [babel, build/test, __test__/]

  - macro: build
    title: Build package
    group: build
    tasks:
      - lint
      - trans/dogma
      - trans/js
      - [fs.cp, package.json, lib/package.json]
      - [fs.cp, README.md, lib/README.md]

  ########
  # test #
  ########

  - alias: redis/start
    title: Start Redis
    group: test
    task: docker.run
    args:
      - redis:6-alpine
      - redis
      - 6379:6379
      - --detach
      - --rm

  - alias: redis/stop
    title: Stop Redis
    group: test
    task: docker.stop
    args: redis

  - tests: test
    title: Run unit tests (Redis must be started)
    group: test
    kind: unit
    workDir: __test__/unit
    tasks:
      - /

  - macro: build-and-test
    title: Build and test (Redis must be started)
    group: test
    tasks:
      - build
      - test

  ######
  # CI #
  ######

  - macro: ci/local
    title: Run the tests locally
    group: ci
    hidden: true
    tasks:
      - build
      - redis/stop
      - redis/start
      - test
      - redis/stop

  - macro: ci/docker
    title: Run the tests into a Docker container
    group: ci
    hidden: true
    tasks:
      - build
      - [docker.rm, "${container}"]
      - [docker.rmi, "${image}"]
      - [docker.build, "${dockerfile}", "${image}"]
      - [docker.run, "${image}", "${container}", 6379:6379]
      - [docker.logs, "${container}", " ok (", true, true]
      - [docker.logs, "${container}", " failed (", false, true]

  - macro: ci
    title: Local Continuous Integration (for running before committing)
    group: ci
    tasks:
      - ci/local
      - ci/docker
```

Vamos a describir los campos de un catálogo:

- `version` (obligatorio), versión actual del catálogo.
  Se recomienda su versionado, de la misma manera que se realiza con proyectos de software.

- `params`, una lista de parámetros que se pueden utilizar en el catálogo y que se reemplazan automáticamente.
  Más adelante hablaremos de ellos.

- `defaultTask`, indica la **tarea predeterminada** (*default task*), esto es, aquella que se ejecutará si no se indica una explícitamente.
  En nuestro ejemplo, la tarea *build-and-test*.

- `plugins`, una lista de plugins a utilizar.
  Sólo es necesario indicar este campo, si se va a utilizar *plugins* que no vienen de fábrica con **EthronLabs**.

- `tasks` (obligatorio), la lista de tareas que el catálogo pone a nuestra disposición.
  En nuestro ejemplo, podemos observar las tareas: *lint*, *build* y *ci*, entre otras.

Un proyecto puede definir tantos catálogos como sea necesario.
Por convenio y buenas prácticas, se usa `Ethron.cat.yaml` como nombre del catálogo principal.
Pero se puede usar otros como, p.ej., `Ethron.dev.yaml` o `Ethron.prod.yaml`.

### Creación de catálogos locales

Como la creación de catálogos locales es muy común, como no podía ser de otra manera, **EthronLabs** presenta un catálogo que proporciona tareas para crearlos fácilmente.
Es decir, automatiza parte de su creación.
Para este fin, se utiliza un **catálogo predefinido** (*built-in catalog*), o sea, un catálogo que viene de fábrica cuando instalamos **EthronLabs**.
Concretamente, se usa `@ethronlabs/cat-catalog`.
Para conocer la lista de tareas que presenta este catálogo:

```
$ ethron -C @ethronlabs/cat-catalog

Catalog: mod://@ethronlabs/cat-catalog

 Task          Group      Desc.                     
----------------------------------------------------
 create-local             Generate a local catalog. 

$
```

Para crear un archivo de catálogo local, ejecute el siguiente comando y responda a las preguntas que se le hacen:

```
$ ll
total 0
$ ethron -C @ethronlabs/cat-catalog r create-local


create-local
------------
Generate local catalog
* Catalog name cat
? Would you like to overwrite the catalog if it exists? Yes
* Catalog version 0.1.0
? Default task name build
  handlebars: render '/home/me/ethronlabs/ethronlabs/ethronlabs/packages/cat-catalog/tmpl/create-local/Ethron.cat.yaml' to 'Ethron.cat.yaml' ok (19 ms)

ok       1
failed   0
skipped  0
total    1
$ ll
total 4
-rw-r--r-- 1 me me 59 may 15 19:21 Ethron.cat.yaml
$ 
```

### Archivo package.json

Cuando un proyecto utiliza un catálogo integrado, generalmente, es necesario definir también un archivo `package.json`.
Este archivo debe definir las dependencias del catálogo como, p. ej., los *plugins* usados, siempre que no sean los que vienen de fábrica.
Por lo general, las dependencias serán de desarrollo, pues el catálogo no es la razón de ser del proyecto.
Sino una herramienta con la que automatizar tareas frecuentemente utilizadas en el proyecto.

## Ejemplos de catálogos locales

A continuación, se lista algunos proyectos que utilizan catálogos locales por si desea echar un vistazo:

- El paquete NPM [@ClouderHub/cache-for-redis](https://bitbucket.org/clouderhub/cache-for-redis/src/master/Ethron.cat.yaml).

- El proyecto de documentación de [EthronLabs](https://bitbucket.org/ethronlabs/ethronlabs-doc/src/master/Ethron.cat.yaml).

## Declaración de tareas en un catálogo local

El campo `tasks` del archivo de catálogo contiene las tareas catalogadas, o sea, las que podemos invocar desde la línea de comandos.
Consiste en una lista de objetos, donde cada objeto contiene una declaración de tarea.
La lista de campos que puede tener cada tarea depende de su tipo.

### Tareas simples

Recordemos que una **tarea simple** (*simple task*) es la unidad básica o más pequeña de trabajo.
Este tipo de tareas tienen los siguientes campos:

- `simple` (texto, obligatorio), sirve para identificar que estamos ante una tarea simple, además de indicar el nombre de la tarea.

- `action` (texto, obligatorio), ruta al módulo que contiene la implementación de la tarea en **JavaScript**.
  Este módulo debe devolver una función que se ejecutará cada vez que se invoque la tarea.

- `desc` (texto), una breve descripción de la tarea.

- `title` (texto), el título que se muestra cuando la tarea se ejecuta.

- `skip` (bool), si debe omitirse su ejecución.
  Valor predeterminado, `false`.

- `onError` (texto), qué hacer si la tarea falla.
  Si `continue`, continuar con la siguiente tarea.
  Si `break`, no ejecutar más tareas.

- `hidden` (bool), indica si es una **tarea oculta** (*hidden task*).
  Este tipo de tareas no se muestra de manera predeterminada cuando se solicita la lista de tareas del catálogo.
  Para listarlas, usar la opción `-a` o `--all`.

- `group` (texto), sirve para agrupar tareas.
  Si se desea listar sólo un determinado grupo de tareas, usar la opción `-g grupo` o `--group grupo`. 

De los campos anteriores, sólo `simple` y `action` son específicos de las tareas simples.
Los restantes también están presentes en los demás tipos de tareas.

### Macros

Una **macro** es una tarea compuesta que contiene una secuencia de tareas que se ejecutan una detrás de otra, en el orden indicado.
Este tipo de tareas presenta los campos comunes, indicados en la sección anterior, más los siguientes:

- `macro` (texto, obligatorio), sirve para indicar que estamos ante una macro, además de su nombre de cara al catálogo.

- `tasks` (lista, obligatorio), las tareas a ejecutar cuando se invoque la macro.
  Si se indica un texto, éste debe hacer referencia a un módulo **JavaScript**.
  Este módulo debe exportar la lista de tareas o bien una función que las devuelva al ser invocada.

He aquí un ejemplo ilustrativo:

```yaml
- macro: ci/docker
  title: Run the tests into a Docker container
  group: ci
  hidden: true
  tasks:
    - build
    - [docker.rm, "${container}"]
    - [docker.rmi, "${image}"]
    - [docker.build, "${dockerfile}", "${image}"]
    - [docker.run, "${image}", "${container}", 6379:6379]
    - [docker.logs, "${container}", " ok (", true, true]
    - [docker.logs, "${container}", " failed (", false, true]
```

Se habla de **macro estática** (*static macro*) cuando la lista de tareas se declara en la propia macro; como el ejemplo anterior.
En cambio, cuando se indica un módulo **JavaScript** que devuelva la lista de tareas, se habla de **macro dinámica** (*dynamic macro*).
Este tipo de macros es muy útil cuando las tareas pueden variar bajo alguna condición.

Las tareas de una macro pueden ser:

- Un nombre de una tarea del catálogo.
  En el ejemplo anterior, *build*.

- Una lista donde el primer elemento es la tarea de un *plugin* y los restantes sus argumentos.
  P.ej., si tenemos `[fs.cp, package.json, lib/package.json]`, la tarea es `cp` del *plugin* `fs` y los argumentos a pasarle son los demás elementos.
  En el ejemplo anterior, todas las demás tareas son de este tipo.

### Aliases

Un **alias** no es más que la declaración de una invocación de tarea con unos determinados argumentos.
Este tipo de tareas presenta los campos comunes, indicados en secciones anteriores, más los siguientes:

- `alias` (texto, obligatorio), sirve para indicar que estamos ante un alias, además de su nombre de cara al catálogo.

- `task` (texto, obligatorio), la tarea a ejecutar.

- `args` (texto, lista, objeto), los argumentos a pasar a la tarea cuando sea llamada.

Ejemplo:

```yaml
- alias: redis/start
  title: Start Redis
  group: test
  task: docker.run
  args:
    - redis:6-alpine
    - redis
    - 6379:6379
    - --detach
    - --rm
```

## Listado de tareas de un catálogo

Se puede listar las tareas de un catálogo, independientemente de su tipo, mediante `ethron`.
De manera predeterminada, si no indicamos ningún comando específico, `ethron` lista las tareas de un catálogo.
A la hora de determinar el catálogo a usar, `ethron` trabaja como sigue:

1. Si el usuario indica la opción `-C`, buscará el catálogo ahí indicado.

2. Si el usuario no ha indicado la opción `-C`, buscará el catálogo indicado en la variable de entorno `$ETHRON_CAT`.

3. Si ninguno de los anteriores se ha especificado, buscará el catálogo `cat`, o sea, `./Ethron.cat.yaml`.

En caso de duda, puede utilizar el comando `ethron env` para conocer los valores actuales de las variables de entorno usadas por `ethron`.
He aquí un ejemplo ilustrativo:

```
$ ethron e

 Variable     Value  Desc.                              
--------------------------------------------------------
 ETHRON_CAT   cat    The default catalog to use.        
 ETHRON_CONF         The default configuration to use.  
 ETHRON_TASK         The default cataloged task to use. 

$
```

Para listar las tareas de un catálogo NPM instalado globalmente, indicaremos su nombre.
Ejemplo:

```
$ ethron -C @ethronlabs/cat-catalog
```

Sólo debe recordar que si va a usar un catálogo NPM, debe haberlo instalado globalmente con `npm`, a menos que sea uno que viene de fábrica con **EthronLabs**.