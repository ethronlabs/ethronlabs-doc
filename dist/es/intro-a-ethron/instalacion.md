# Instalación

*Tiempo estimado de lectura: 1min*

**EthronLabs** está escrito en [Dogma](http://dogmalang.com) y compilado a **JavaScript** para [Node.js](https://nodejs.org).

## Dependencias

**Node.js** y **NPM**.

## Instalación de EthronLabs

Una buena instalación inicial puede consistir en ejecutar lo siguiente:

```
$ npm i -g --production @ethronlabs/cli
```

Una vez instalado, es buena práctica comprobar que se tiene acceso al comando `ethron`:

```
$ ethron help
```