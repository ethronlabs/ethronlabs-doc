# Introducción a EthronLabs

1. [Introducción](intro.md)
2. [Instalación](instalacion.md)
3. [Introducción a las tareas](intro-a-las-tareas.md)
4. [Catálogos locales](catalogos-locales.md)
5. [Ejecución de tareas](ejecucion-de-tareas.md)
6. [Parámetros de catálogo](parametros-de-catalogo.md)
7. [Plugins predefinidos](plugins-predefinidos.md)