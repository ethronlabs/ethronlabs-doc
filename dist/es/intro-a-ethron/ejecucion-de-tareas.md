# Ejecución de tareas

*Tiempo estimado de lectura: 3min*

El componente de **EthronLabs** que se encarga de ejecutar las tareas se conoce formalmente como **ejecutor** (*runner*).
Es el encargado de recibir una tarea y ejecutarla, devolviendo un resultado que describe su ejecución.

## Ejecución de tareas de un catálogo

Las tareas se ejecutan con el comando `ethron r` o `ethron run`.
Puede usar la siguiente línea para obtener su ayuda:

```
$ ethron r --help
```

Groso modo, para ejecutar una tarea tenemos que saber:

- El catálogo a utilizar.
  Si no es el predeterminado, indicarlo con la opción `-C` (*C* mayúscula) o la variable de entorno `$ETHRON_CAT`.

- El nombre de la(s) tarea(s) a ejecutar.

- Los argumentos a pasar a la(s) tarea(s) a ejecutar.

A continuación, se muestra cómo solicitar que se ejecuten varias tareas de un catálogo local:

```
$ ethron r build redis/start test redis/stop


build
-----
Build package
  Lint source code ok (110 ms)
  Transpile from Dogma to JS
    fs: remove 'build' ok (2 ms)
    shell: execute 'dogmac js -o build src/' ok (52 ms)
    shell: execute 'dogmac js -o build test/' ok (90 ms)
  Transpile from JS to JS
    fs: remove 'lib' ok (1 ms)
    fs: remove '__test__' ok (0 ms)
    babel: transpile 'build/src' to 'lib/' ok (570 ms)
    babel: transpile 'build/test' to '__test__/' ok (94 ms)
  fs: copy 'package.json' to 'lib/package.json' ok (2 ms)
  fs: copy 'README.md' to 'lib/README.md' ok (1 ms)

ok       10
failed   0
skipped  0
total    10

redis/start
-----------
Start Redis ok (503 ms)

ok       1
failed   0
skipped  0
total    1

test
----
Run unit tests (Redis must be started)
  /home/me/ClouderHub/code/cache-for-redis/__test__/unit/index.js
    API
      act(ion) ok (1 ms)
  /home/me/ClouderHub/code/cache-for-redis/__test__/unit/JsonCache.js
    get()
      init: connect cache object ok (8 ms)
      return nil when not existing
        act(ion) ok (2 ms)
      when key existing
        return value as text
          setup: ensure key existing ok (3 ms)
          act(ion) ok (2 ms)
        ttl to set
          ttl = true
            act(ion) ok (1 ms)
          ttl is num
            act(ion) ok (1 ms)
      return value as object when existing
        init: add key value ok (1 ms)
        act(ion) ok (0 ms)
      fin: disconnect cache object ok (0 ms)
    set()
      init: connect cache object ok (2 ms)
      set value when non-existing key
        init: ensure key not existing ok (2 ms)
        act(ion) ok (2 ms)
      set value when existing
        init: ensure key existing ok (1 ms)
        act(ion) ok (1 ms)
      set value with ttl
        act(ion) ok (1 ms)
      fin: disconnect cache object ok (0 ms)
  /home/me/ClouderHub/code/cache-for-redis/__test__/unit/TextCache.js
    connect()
      Redis found
        init: create cache object ok (0 ms)
        act(ion) ok (2 ms)
        teardown: disconnect cache object ok (0 ms)
      Redis not found
        act(ion) ok (2 ms)
        teardown: disconnect cache object ok (0 ms)
      error when already connected
        init: connect cache object ok (2 ms)
        act(ion) ok (0 ms)
        teardown: disconnect cache object ok (1 ms)
    disconnect()
      when connected
        setup: create cache object ok (0 ms)
        init: connect ok (2 ms)
        act(ion) ok (0 ms)
      when not connected
        setup: create cache object ok (1 ms)
        act(ion) ok (0 ms)
    get()
      init: connect cache object ok (2 ms)
      when key existing
        return value as text
          setup: ensure key existing ok (2 ms)
          act(ion) ok (1 ms)
        ttl to set
          ttl = true
            act(ion) ok (1 ms)
          ttl is num
            act(ion) ok (1 ms)
      return nil when not existing
        act(ion) ok (1 ms)
      error when not connected
        act(ion) ok (0 ms)
      fin: disconnect cache object ok (1 ms)
    set()
      init: connect cache object ok (1 ms)
      set value when non-existing key
        init: ensure key not existing ok (1 ms)
        act(ion) ok (1 ms)
      set value when existing
        init: ensure key existing ok (0 ms)
        act(ion) ok (1 ms)
      setting ttl
        when ttl is true
          act(ion) ok (1 ms)
        when ttl is num
          act(ion) ok (2 ms)
      error when not connected
        act(ion) ok (1 ms)
      fin: disconnect cache object ok (0 ms)
    remove()
      init: connect cache object ok (2 ms)
      not error when not existing
        act(ion) ok (1 ms)
      when existing
        init: create key value ok (1 ms)
        act(ion) ok (0 ms)
      error when not connected
        act(ion) ok (1 ms)
      fin: disconnect cache object ok (0 ms)

ok       53
failed   0
skipped  0
total    53

redis/stop
----------
Stop Redis ok (443 ms)

ok       1
failed   0
skipped  0
total    1
$
```

Observe que la ejecución de cada tarea invocada con `ethron r` se reporta en su propio *informe*.

### Ejecución de la tarea predeterminada

Recordemos que todo catálogo tiene la posibilidad de definir una **tarea predeterminada** (*default task*), aquélla que se ejecutará cuando no se indique ninguna explícitamente en el comando `ethron r`.
Cuando se lista las tareas del catálogo, ésta aparece con el sufijo `*`.
Veámoslo mediante un ejemplo:

```
$ ethron

Catalog: ./Ethron.cat.yaml

 Task              Group  Desc.                                                        
---------------------------------------------------------------------------------------
 build             build  Build package                                                
 build-and-test *  test   Build and test (Redis must be started)                       
 ci                ci     Local Continuous Integration (for running before committing) 
 lint              build  Lint source code                                             
 redis/start       test   Start Redis                                                  
 redis/stop        test   Stop Redis                                                   
 test              test   Run unit tests (Redis must be started)                       

$
```

Y ahora, un ejemplo de su invocación:

```
$ ethron r
```

### Paso de argumentos a las tareas

Si fuera necesario, se puede pasar argumentos a las tareas.
Para ello, se usa el siguiente formato:

```
nombreTarea,arg1=valor,arg2=valor...
```

Hay que tener en cuenta lo siguiente:

- El nombre de la tarea se separa de sus argumentos por una coma (`,`).

- Los argumentos son de tipo objeto, esto es, pares `nombre=valor`.

- Los argumentos se separan entre sí también por comas.

- No se puede indicar espacios ni comas en los valores de los parámetros.

Vamos a ver un ejemplo... Recordemos que se puede crear un archivo de catálogo local mediante `@ethronlabs/cat-catalog`.
Concretamente mediante su tarea `create-local`.
Esta tarea es de tipo generador, todavía no presentada formalmente.
Pero tiene la capacidad de hacerle preguntas al usuario y trabajar con sus respuestas para crear el archivo del catálogo.
Si lo deseamos, podemos omitir el modo interactivo e invocar la tarea proporcionando las respuestas desde la línea de comandos.
Para ello, en primer lugar, veamos cómo obtener las preguntas que realiza el generador, basta con indicar el catálogo y el nombre de la tarea:

```
$ ethron -C @ethronlabs/cat-catalog create-local

Catalog: mod://@ethronlabs/cat-catalog

Task: create-local

 Name         Title                                                  Prompt   Options    Value     
---------------------------------------------------------------------------------------------------
 name*        Catalog name                                           input               cat       
 overwrite    Would you like to overwrite the catalog if it exists?  confirm             false     
 version*     Catalog version                                        input               0.1.0     
 defaultTask  Default task name                                      input                         

$
```

Cada pregunta tiene un nombre y un título.
Aquellas preguntas cuyos nombres presentan un sufijo `*` son obligatorias; las restantes opcionales.
Pues vamos a usar los nombres para construir un objeto argumento a pasar a la tarea con objeto de que no nos haga las preguntas.
He aquí un ejemplo:

```
$ ethron r -C @ethronlabs/cat-catalog create-local,name=cat,overwrite=true,version=0.1.0,defaultTask=
```

Cuando la pregunta es opcional, podemos indicar la ausencia de valor mediante `nombre=`.