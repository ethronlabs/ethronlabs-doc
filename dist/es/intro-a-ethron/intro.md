# Introducción a EthronLabs

*Tiempo estimado de lectura: 6min*

**EthronLabs** es una suite de automatización de TI.
Proporciona varias funcionalidades bajo una misma API sencilla, reduciendo así la curva de aprendizaje.

Presenta las siguientes características:

- Se puede usar **gratuitamente**.

- Es **fácil de aprender y de usar**.

- Proporciona distintos tipos de de tareas para **adaptarse a las necesidades de los usuarios** como, p.ej., tareas simples y tareas compuestas.

- Permite automatizar **pruebas de software**, al igual que hacen *Jasmine*, *Mocha* y *Jest*.

- Proporciona una **biblioteca de aserciones** al igual que *Should* y *Chai*.

- Se puede **extender su funcionalidad** mediante **JavaScript** y **Dogma** usando **plugins** y **catálogos**.

En el *roadmap* del proyecto, tenemos las siguientes características en mente:

- Facilitar la prueba de contractos de **blockchain** tal como también permite **Truffle** y **Embark**.

- Soporte integrado para **SSH**, lo que permitirá ejecutar tareas en **máquinas remotas** tal como hacen **Ansible** y **Salt**.

- Soporte de tareas de **colas**, lo que permitirá enviar y recibir mensajes de colas **Redis**, **ZeroMQ** y **PostgreSQL**.

- Notificación de la ejecución de tareas a colas para su consumo, p. ej., para su presentación o almacenamiento en bases de datos.

- Incremento de plugins y catálogos reutilizables.

- Consola web desde la que lanzar la ejecución de tareas y recibir notificación de su evolución y resultado.

- Soporte para el **navegador**.

- Soporte para **Windows** y **Raspberry Pi**.

Aunque puede utilizarse en varios ámbitos, está dirigido principalmente a:

- **Desarrolladores de software** de todo tipo de aplicaciones como, p.ej., **Node.js**, **Go** o **blockchain**.

- **Administradores de sistemas**.

- **DevOps**.

- **Administradores de bases de datos**.

### Requisitos

Es indispensable conocer [YAML](https://yaml.org/).
Si además se desea realizar *plugins*, es necesario tener conocimiento de **JavaScript** y [Node.js](https://nodejs.org).

## Qué es la automatización TI

La **automatización** (*automation*) no es más que el proceso mediante el cual se convierte tareas manuales repetitivas en automáticas.
Al automatizarlas, podemos ejecutarlas más fácil y rápidamente que si tuviéramos que hacerlo una y otra vez manualmente.
Por su parte, la **automatización TI** (*IT automation*) es aquella que se centra en automatizar tareas y/o procesos de TI (tecnologías de la información).

Entre los principales usos de la automatización TI, encontramos:

- Construcción de software.

- Integración continua (CI, *continuous integration*).

- Despliegue continuo (CD, *continuous delivery* o *continuous deployment*).

- Realización de pruebas.

- Administración y monitorización de configuración.

- Extracción, transformación y carga de datos (ETL).

- Generación de archivos y/o directorios a partir de plantillas.

- Realización de copias de seguridad.

Y entre las principales ventajas:

- Aumento de la productividad.

- Aumento de la calidad.

- Aumento de la precisión o exactitud.

- Aumento de la robustez.

- Aumento de la reutilización.

- Reducción de costes.

- Reducción de errores.

- Reducción de riesgos.

- Reducción de tareas manuales.

- Reducción de tiempos de ejecución, de operación, de despliegue...

- Reducción de mantenimiento.

- Reducción de la duplicidad de trabajos.

- Reducción de la complejidad.

**EthronLabs** se ha desarrollado en [Dogma](http://dogmalang.com) y compilado a **JavaScript** para su uso con [Node.js](https://nodejs.org).
Requiere menor esfuerzo y menor curva de aprendizaje.
Se puede usar localmente y, en un futuro, en *cloud*.

## Arquitectura

La **arquitectura** (*architecture*) describe el conjunto y la estructura de componentes de algo, en nuestro caso, de **EthronLabs**.
Tenemos básicamente los siguientes elementos:

- Las **tareas**, las cuales representan trabajos o actividades a ejecutar como, p.ej., una operación de compilación, de despliegue, etc.

- El **motor de tareas**, el programa que ejecuta y supervisa las tareas.

- El **notificador de resultados** o simplemente **notificador**, aquel que informa de las tareas en ejecución, mostrando el tiempo que tardaron y su estado final.
  P.ej., a través de una consola o de una web.

- Los **catálogos de tareas**, contenedores de tareas empaquetadas para su ejecución desde línea de comandos o una consola web.

- Los **plugins**, los cuales proporcionan tareas reutilizables como, p.ej., para trabajar con Docker, archivos, compilar, desplegar, etc.

A continuación, se muestra la salida de `ethron` para la lista de tareas de un catálogo:

```
$ ethron

Catalog: ./Ethron.cat.yaml

 Task              Group  Desc.                                                        
---------------------------------------------------------------------------------------
 build             build  Build package                                                
 build-and-test *  test   Build and test (Redis must be started)                       
 ci                ci     Local Continuous Integration (for running before committing) 
 lint              build  Lint source code                                             
 redis/start       test   Start Redis                                                  
 redis/stop        test   Stop Redis                                                   
 test              test   Run unit tests (Redis must be started)                       

$
```

Y a continuación, un ejemplo de ejecución de una tarea:

```
$ ethron r build


build
-----
Build package
  Lint source code ok (124 ms)
  Transpile from Dogma to JS
    fs: remove 'build' ok (3 ms)
    shell: execute 'dogmac js -o build src/' ok (53 ms)
    shell: execute 'dogmac js -o build test/' ok (86 ms)
  Transpile from JS to JS
    fs: remove 'lib' ok (1 ms)
    fs: remove '__test__' ok (0 ms)
    babel: transpile 'build/src' to 'lib/' ok (691 ms)
    babel: transpile 'build/test' to '__test__/' ok (85 ms)
  fs: copy 'package.json' to 'lib/package.json' ok (2 ms)
  fs: copy 'README.md' to 'lib/README.md' ok (1 ms)

ok       10
failed   0
skipped  0
total    10
$
```

He aquí la definición de un catálogo de tareas de ejemplo:

```yaml
###################
# Ethron.cat.yaml #
###################
version: 1.0.0

params:
  - param: dockerfile
    value: ./Dockerfile

  - param: image
    value: cache-for-redis

  - param: container
    value: cache-for-redis

defaultTask: build-and-test

tasks:
  #########
  # build #
  #########

  - alias: lint
    title: Lint source code
    group: build
    task: exec
    args: dogmac check src test

  - macro: trans/dogma
    title: Transpile from Dogma to JS
    group: build
    hidden: true
    tasks:
      - [fs.rm, build]
      - [exec, "dogmac js -o build src/"]
      - [exec, "dogmac js -o build test/"]

  - macro: trans/js
    title: Transpile from JS to JS
    group: build
    hidden: true
    tasks:
      - [fs.rm, lib]
      - [fs.rm, __test__]
      - [babel, build/src, lib/]
      - [babel, build/test, __test__/]

  - macro: build
    title: Build package
    group: build
    tasks:
      - lint
      - trans/dogma
      - trans/js
      - [fs.cp, package.json, lib/package.json]
      - [fs.cp, README.md, lib/README.md]

  ########
  # test #
  ########

  - alias: redis/start
    title: Start Redis
    group: test
    task: docker.run
    args:
      - redis:6-alpine
      - redis
      - 6379:6379
      - --detach
      - --rm

  - alias: redis/stop
    title: Stop Redis
    group: test
    task: docker.stop
    args: redis

  - tests: test
    title: Run unit tests (Redis must be started)
    group: test
    kind: unit
    workDir: __test__/unit
    tasks:
      - /

  - macro: build-and-test
    title: Build and test (Redis must be started)
    group: test
    tasks:
      - build
      - test

  ######
  # CI #
  ######

  - macro: ci/local
    title: Run the tests locally
    group: ci
    hidden: true
    tasks:
      - build
      - redis/stop
      - redis/start
      - test
      - redis/stop

  - macro: ci/docker
    title: Run the tests into a Docker container
    group: ci
    hidden: true
    tasks:
      - build
      - [docker.rm, "${container}"]
      - [docker.rmi, "${image}"]
      - [docker.build, "${dockerfile}", "${image}"]
      - [docker.run, "${image}", "${container}", 6379:6379]
      - [docker.logs, "${container}", " ok (", true, true]
      - [docker.logs, "${container}", " failed (", false, true]

  - macro: ci
    title: Local Continuous Integration (for running before committing)
    group: ci
    tasks:
      - ci/local
      - ci/docker
```
